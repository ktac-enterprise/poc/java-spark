package com.kouasseu;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;

import static com.kouasseu.pratice.Exercise.haveTopTenTopicOnGivingFile;
import static com.kouasseu.pratice.Exercise.rankingVideosTrainingCoursesByNumberOfViews;
import static com.kouasseu.utils.Common.disableLogInfos;
import static com.kouasseu.utils.SparkFunction.*;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  20/10/2023 -- 08:36<br></br>
 * By : @author alexk<br></br>
 * Project : Default (Template) Project<br></br>
 * Package : com.kouasseu<br></br>
 */

public class Main {
    public static void main(String[] args) {

        disableLogInfos().run();
        System.setProperty("hadoop.home.dir", "C:\\devtools\\winutils");

        SparkConf conf = new SparkConf()
                .setAppName("Alex-Spark-App")
                .setMaster("local[*]");

        try (JavaSparkContext sc = new JavaSparkContext(conf)) {
            reduceRDDOperation(sc).run();
            mapRDDOperation(sc).run();
            customTupleObjectRDD(sc).run();
            tupleRDD(sc).run();
            pairRDD(sc).run();
            pairRDDGroupByKey(sc).run();
            flatMapRdd(sc).run();
            filterOnRdd(sc).run();
            readFromDisk(sc).run();
            haveTopTenTopicOnGivingFile(sc).run();
            rankingVideosTrainingCoursesByNumberOfViews(sc).run();
            reduceByKeyPerformanceImprovement(sc).run();
        }

    }


}
