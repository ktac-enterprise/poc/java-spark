package com.kouasseu.pratice;

import com.kouasseu.Util;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import scala.Tuple2;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  20/10/2023 -- 23:08<br></br>
 * By : @author alexk<br></br>
 * Project : pocJspark<br></br>
 * Package : com.kouasseu.pratice<br></br>
 */
public class Exercise {
    public static Runnable haveTopTenTopicOnGivingFile(JavaSparkContext sc) {
        return () -> {
            JavaRDD<String> input = sc.textFile("src/main/resources/subtitles/input-spring.txt");
            JavaRDD<String> removeNonCharLetters = input.map(word -> word.replaceAll("[^a-zA-Z\\s]", "").toLowerCase());
            JavaRDD<String> removeBlankSentences = removeNonCharLetters.filter(word -> !word.trim().isEmpty() || !word.trim().isBlank());

            JavaPairRDD<Long, String> sortedWords = removeBlankSentences.flatMap(word -> Arrays.asList(word.split(" ")).iterator())
                    .filter(Util::isNotBoring).filter(v1 -> !v1.isEmpty())
                    .mapToPair(word -> new Tuple2<>(word, 1L))
                    .reduceByKey(Long::sum)
                    .mapToPair(tuple -> new Tuple2<>(tuple._2, tuple._1))
                    .sortByKey(false);


            List<Tuple2<Long, String>> take = sortedWords.take(10);

            take.forEach(System.out::println);

            Scanner scanner = new Scanner(System.in);
            scanner.nextLine();
        };
    }

    public static Runnable rankingVideosTrainingCoursesByNumberOfViews(JavaSparkContext sc) {
        boolean testMode = false;
        return  () -> {
            JavaPairRDD<Integer, Integer> viewsPair = buildViewsDataRdd(sc, testMode);
            viewsPair = viewsPair.distinct();
            JavaPairRDD<Integer, Integer> chapterDataPair = buildChapterDataRdd(sc, testMode);
            JavaPairRDD<Integer, String> titlesPair = buildTitlesDataRdd(sc, testMode);

            JavaPairRDD<Integer, Integer> courseChapterCount = chapterDataPair.mapToPair(chapter -> new Tuple2<>(chapter._2, 1))
                    .reduceByKey(Integer::sum);

            JavaPairRDD<Integer, Integer> viewsChapterKey = viewsPair.mapToPair(view -> new Tuple2<>(view._2, view._1));

            JavaPairRDD<Integer, Tuple2<Integer, Integer>> viewChapterCoursePair = viewsChapterKey.join(chapterDataPair);

            JavaPairRDD<Tuple2<Integer, Integer>, Integer> userCourseViewsCount = viewChapterCoursePair.mapToPair(viewCourse -> new Tuple2<>(viewCourse._2, 1))
                    .reduceByKey(Integer::sum);

            JavaPairRDD<Integer, Integer> courseViewCount = userCourseViewsCount.mapToPair(userCourseView -> new Tuple2<>(userCourseView._1._2, userCourseView._2));

            JavaPairRDD<Integer, Tuple2<Integer, Integer>> courseViewCountAmountTotalCourseChapter = courseViewCount.join(courseChapterCount);

            JavaPairRDD<Integer, Double> courseViewPercentage = courseViewCountAmountTotalCourseChapter
                    .mapValues(value -> value._1.doubleValue() / value._2.doubleValue());

            JavaPairRDD<Integer, Long> courseScore = courseViewPercentage.mapValues(buildScoreFromPercent())
                    .reduceByKey(Long::sum);

            JavaPairRDD<Long, String> niceCourseScore = courseScore.join(titlesPair)
                    .mapToPair(courseTitle -> new Tuple2<>(courseTitle._2._1, courseTitle._2._2))
                    .sortByKey(false);

            niceCourseScore.collect().forEach(System.out::println);


            Scanner scanner = new Scanner(System.in);
            scanner.nextLine();
        };
    }

    private static Function<Double, Long> buildScoreFromPercent() {
        return percent -> {
          if (percent > 0.9) return 10L;
          if (percent > 0.5) return 4L;
          if (percent > 0.25) return 2L;
          return 0L;
        };
    }

    private static JavaPairRDD<Integer, Integer> buildChapterDataRdd(JavaSparkContext sc, boolean testMode) {
        if (testMode) {
            List<Tuple2<Integer, Integer>> chapterData = List.of(
                    new Tuple2<>(96, 1),
                    new Tuple2<>(97, 1),
                    new Tuple2<>(98, 1),
                    new Tuple2<>(99, 2),
                    new Tuple2<>(100, 3),
                    new Tuple2<>(101, 3),
                    new Tuple2<>(102, 3),
                    new Tuple2<>(103, 3),
                    new Tuple2<>(104, 3),
                    new Tuple2<>(105, 3),
                    new Tuple2<>(106, 3),
                    new Tuple2<>(107, 3),
                    new Tuple2<>(108, 3),
                    new Tuple2<>(109, 3)
            );
            return sc.parallelizePairs(chapterData);
        }

        return sc.textFile("src/main/resources/viewingfigures/chapters.csv")
                .mapToPair(commaSeparatedLine -> {
                    String[] cols = commaSeparatedLine.split(",");
                    return new Tuple2<>(Integer.parseInt(cols[0]), Integer.parseInt(cols[1]));
                }).cache();
    }


    private static JavaPairRDD<Integer, Integer> buildViewsDataRdd(JavaSparkContext sc, boolean testMode) {
        if (testMode) {
            List<Tuple2<Integer, Integer>> views = List.of(
                    new Tuple2<>(14, 96),
                    new Tuple2<>(14, 97),
                    new Tuple2<>(13, 96),
                    new Tuple2<>(13, 96),
                    new Tuple2<>(13, 96),
                    new Tuple2<>(14, 99),
                    new Tuple2<>(13, 100)
            );
            return sc.parallelizePairs(views);
        }

        return sc.textFile("src/main/resources/viewingfigures/views-*.csv")
                .mapToPair(commaSeparatedLine -> {
                    String[] cols = commaSeparatedLine.split(",");
                    return new Tuple2<>(Integer.parseInt(cols[0]), Integer.parseInt(cols[1]));
                });
    }


    private static JavaPairRDD<Integer, String> buildTitlesDataRdd(JavaSparkContext sc, boolean testMode) {
        if (testMode) {

            List<Tuple2<Integer, String>> titles = List.of(
                    new Tuple2<>(1,"HTML5"),
                    new Tuple2<>(2,"Java Fundamentals"),
                    new Tuple2<>(3,"Wildfly 2")
            );
            return sc.parallelizePairs(titles);
        }

        return sc.textFile("src/main/resources/viewingfigures/titles.csv")
                .mapToPair(commaSeparatedLine -> {
                    String[] cols = commaSeparatedLine.split(",");
                    return new Tuple2<>(Integer.parseInt(cols[0]), cols[1]);
                });
    }
}
