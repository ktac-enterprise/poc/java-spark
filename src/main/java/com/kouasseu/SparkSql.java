package com.kouasseu;

import org.apache.spark.sql.SparkSession;

import static com.kouasseu.utils.Common.disableLogInfos;
import static com.kouasseu.utils.SparSqlFunction.*;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  24/10/2023 -- 22:30<br></br>
 * By : @author alexk<br></br>
 * Project : pocJspark<br></br>
 * Package : com.kouasseu<br></br>
 */
public class SparkSql {
    public static void main(String[] args) {

        disableLogInfos().run();

        System.setProperty("hadoop.home.dir", "C:\\devtools\\winutils");

        try(SparkSession ss = SparkSession.builder()
                .appName("Alex-SparkSql-App")
                .master("local[*]")
                .config("spark.sql.warehouse.dir", "file:///d:/app/tmp/")
                .getOrCreate()) {

            sparkSqlUnderstandSparkUIGraph(ss).run();
        }
    }
}
