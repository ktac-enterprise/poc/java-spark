package com.kouasseu.utils;

import com.google.common.collect.Iterables;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.storage.StorageLevel;
import org.jetbrains.annotations.NotNull;
import scala.Tuple2;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.DoubleSupplier;
import java.util.function.IntSupplier;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  20/10/2023 -- 23:17<br></br>
 * By : @author alexk<br></br>
 * Project : pocJspark<br></br>
 * Package : com.kouasseu.utils<br></br>
 */
public class SparkFunction {

    static Consumer<Object> print() {
        return o -> System.out.printf("Result --->>> %s\n", o);
    }

    public static Runnable reduceRDDOperation(JavaSparkContext sc) {
        List<Double> inputData = new ArrayList<>();
        Random random = new Random(124L);
        DoubleSupplier newDouble = random::nextDouble;
        for (int i = 0; i < 1000; i++) {
            inputData.add(newDouble.getAsDouble());
        }

        return () -> {
            Double result = sc.parallelize(inputData).reduce(sumDouble());
            print().accept(result);
        };
    }

    public static Function2<Double, Double, Double> sumDouble() {
        return Double::sum;
    }

    public static Runnable mapRDDOperation(JavaSparkContext sc) {
        List<Integer> inputData = getIntegers();

        return () -> {
            JavaRDD<Double> sqrtRdd = sc.parallelize(inputData).map(sqrtInteger());

            //sqrtRdd.foreach(aDouble -> print().accept(aDouble));

            //print().accept(sqrtRdd.count());

            print().accept(
                    sqrtRdd.map(v1 -> 1L)
                            .reduce(Long::sum)
            );
        };
    }


    public static Runnable customTupleObjectRDD(JavaSparkContext sc) {
        List<Integer> inputData = getIntegers();

        return () -> {
            JavaRDD<IntegerWithSquareRoot> sqrtRdd = sc.parallelize(inputData).map(toIntSqrtObject());
            print().accept(sqrtRdd.count());
        };
    }

    public static Runnable tupleRDD(JavaSparkContext sc) {
        List<Integer> inputData = getIntegers();

        return () -> sc.parallelize(inputData)
                .map(toTuple2()).foreach(System.out::println);
    }

    public static Runnable pairRDD(JavaSparkContext sc) {
        return () -> sc.parallelize(getLogsStrings())
                .mapToPair(getTupleKeyValue())
                .reduceByKey(Long::sum)
                .foreach(tuple -> print().accept(tuple));
    }

    public static Runnable pairRDDGroupByKey(JavaSparkContext sc) {
        return () -> sc.parallelize(getLogsStrings())
                .mapToPair(getTupleKeyValue())
                .groupByKey()
                .foreach(tuple -> print().accept(tuple._2.spliterator().getExactSizeIfKnown()));
    }


    public static Runnable flatMapRdd(JavaSparkContext sc) {
        return () -> sc.parallelize(getLogsStrings())
                .flatMap(value -> Arrays.asList(value.split(" ")).iterator())
                .foreach(word -> print().accept(word));
    }

    public static Runnable filterOnRdd(JavaSparkContext sc) {
        return () -> sc.parallelize(getLogsStrings())
                .flatMap(value -> Arrays.asList(value.split(" ")).iterator())
                .filter(word -> word.length() > 1)
                .foreach(word -> print().accept(word));
    }

    public static Runnable readFromDisk(JavaSparkContext sc) {
        return () -> sc.textFile("src/main/resources/subtitles/input.txt")
                .flatMap(value -> Arrays.asList(value.split(" ")).iterator())
                .filter(word -> word.length() > 1)
                .foreach(word -> print().accept(word));
    }

    public static Runnable reduceByKeyPerformanceImprovement(JavaSparkContext sc) {
        return () -> {
            JavaRDD<String> initialRdd = sc.textFile("src/main/resources/bigLog.txt");

            System.out.println("Initial RDD Partition Size: " + initialRdd.getNumPartitions());

            JavaPairRDD<String, String> warningsAgainstDate = initialRdd.mapToPair(inputLine -> {
                String[] cols = inputLine.split(":");
                String level = cols[0];
                String date = cols[1];
                return new Tuple2<>(level, date);
            });

            System.out.println("After a narrow transformation we have " + warningsAgainstDate.getNumPartitions() + " parts");

            // Now we're going to do a "wide" transformation
            JavaPairRDD<String, Iterable<String>> results = warningsAgainstDate.groupByKey();

            //results = results.cache(); //Peut générer une exception s'il n'y a pas assez d'espace dans la mémoire pour cacher cette donnée
            results = results.persist(StorageLevel.MEMORY_AND_DISK());

            System.out.println(results.getNumPartitions() + " partitions after the wide transformation");

            results.foreach(it -> System.out.println("key " + it._1 + " has " + Iterables.size(it._2) + " elements"));

            System.out.println(results.count());

            Scanner scanner = new Scanner(System.in);
            scanner.nextLine();
        };
    }

    /**
     * Functions
     */


    @NotNull
    private static List<Integer> getIntegers() {
        List<Integer> inputData = new ArrayList<>();
        Random random = new Random(10000L);
        IntSupplier nextInt = random::nextInt;
        for (int i = 0; i < 1000; i++) {
            inputData.add(nextInt.getAsInt());
        }
        inputData.removeIf(integer -> integer < 0 );
        return inputData;
    }


    static Function<Integer, Double> sqrtInteger() {
        return Math::sqrt;
    }

    static Function<Integer, IntegerWithSquareRoot> toIntSqrtObject() {
        return IntegerWithSquareRoot::new;
    }
    static Function<Integer, Tuple2<Integer, Double>> toTuple2() {
        return v1 -> new Tuple2<>(v1, Math.sqrt(v1));
    }


    @NotNull
    private static PairFunction<String, String, Long> getTupleKeyValue() {
        return log -> {
            String[] lines = log.split(":");
            String level = lines[0];
            String date = lines[1];
            return new Tuple2<>(level, 1L);
        };
    }



    @NotNull
    private static List<String> getLogsStrings() {
        List<String > inputData = new ArrayList<>();

        inputData.add("WARN: Tuesday 4 September 0405");
        inputData.add("ERROR: Tuesday 4 September 0408");
        inputData.add("WARN: Tuesday 4 September 0405");
        inputData.add("WARN: Wednesday 5 September 1245");
        inputData.add("FATAL: Wednesday 5 September 8419");
        inputData.add("ERROR: Wednesday 5 September 8542");
        inputData.add("FATAL: Friday 7 September 3245");
        inputData.add("ERROR: Saturday 8 September 3214");
        inputData.add("WARN: Sunday 9 September 3874");
        inputData.add("WARN: Sunday 9 September 7452");

        return inputData;
    }

}


record IntegerWithSquareRoot(int i, double sqrt) {
    public IntegerWithSquareRoot(int i) {
        this(i, Math.sqrt(i));
    }
}