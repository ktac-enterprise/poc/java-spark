package com.kouasseu.utils;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.Collections;
import java.util.List;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  24/10/2023 -- 22:30<br></br>
 * By : @author alexk<br></br>
 * Project : pocJspark<br></br>
 * Package : com.kouasseu.utils<br></br>
 */
public class Common {

    public static Runnable disableLogInfos() {
        return () -> {
            List<Logger> loggers = Collections.<Logger>list(LogManager.getCurrentLoggers());
            loggers.add(LogManager.getRootLogger());
            for (Logger logger : loggers) {
                logger.setLevel(Level.WARN);
            }
//            Logger.getLogger("org.apache.log4j").setLevel(Level.WARN);
        };
    }
}
