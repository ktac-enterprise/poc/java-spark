package com.kouasseu.utils;

import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.api.java.UDF1;
import org.apache.spark.sql.api.java.UDF2;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import static org.apache.spark.sql.functions.*;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  24/10/2023 -- 22:32<br></br>
 * By : @author alexk<br></br>
 * Project : pocJspark<br></br>
 * Package : com.kouasseu.utils<br></br>
 */
public class SparSqlFunction {

    public static Runnable introSparkSql(SparkSession ss) {
        return () -> {
            Dataset<Row> dataset = ss.read()
                    .option("header", true)
                    .csv("src/main/resources/exams/students.csv");

            dataset.show();

            long numberOfRows = dataset.count();
            System.out.println("There are " + numberOfRows + " records");
        };
    }

    public static Runnable sparkSqlDatasetBasic(SparkSession ss) {
        return () -> {
            Dataset<Row> dataset = ss.read()
                    .option("header", true)
                    .csv("src/main/resources/exams/students.csv");

            dataset.show();

            long numberOfRows = dataset.count();
            System.out.println("There are " + numberOfRows + " records");

            Row firstRow = dataset.first();

            var subject = firstRow.getAs("subject");
            System.out.println(subject);

            var year = firstRow.getAs("year");
            System.out.println(year);
        };
    }

    public static Runnable sparkSqlDatasetFiltersString(SparkSession ss) {
        return () -> {
            Dataset<Row> dataset = ss.read()
                    .option("header", true)
                    .csv("src/main/resources/exams/students.csv");

            Dataset<Row> modernArtResults = dataset.filter("subject = 'Modern Art' AND year >= 2007");

            modernArtResults.show();

            long numberOfRows = modernArtResults.count();
            System.out.println("There are " + numberOfRows + " records");

        };
    }

    public static Runnable sparkSqlDatasetFiltersLambda(SparkSession ss) {
        return () -> {
            Dataset<Row> dataset = ss.read()
                    .option("header", true)
                    .csv("src/main/resources/exams/students.csv");

            Dataset<Row> modernArtResults = dataset.filter(modernArtFilter());

            modernArtResults.show();

            long numberOfRows = modernArtResults.count();
            System.out.println("There are " + numberOfRows + " records");

        };
    }

    public static Runnable sparkSqlDatasetFiltersColumn(SparkSession ss) {
        return () -> {
            Dataset<Row> dataset = ss.read()
                    .option("header", true)
                    .csv("src/main/resources/exams/students.csv");

            Dataset<Row> modernArtResults = dataset.filter(
                    col("subject").equalTo("Modern Art")
                            .and(col("year").geq(2007)));

            modernArtResults.show();

            long numberOfRows = modernArtResults.count();
            System.out.println("There are " + numberOfRows + " records");

        };
    }

    public static Runnable sparkSqlDatasetFullSyntax(SparkSession ss) {
        return () -> {
            Dataset<Row> dataset = ss.read()
                    .option("header", true)
                    .csv("src/main/resources/exams/students.csv");

            dataset.createOrReplaceTempView("temp_students"); //créer un vue qu'on peut utiliser dans une requête SQL

            Dataset<Row> result = ss.sql("select max(score), min(score), avg(score) from temp_students where subject = 'French' order by year desc");


            result.show();

            long numberOfRows = result.count();
            System.out.println("There are " + numberOfRows + " records");

        };
    }

    public static Runnable sparkSqlInMemoryData(SparkSession ss) {
        return () -> {

            List<Row> inMemory = new ArrayList<>();

            inMemory.add(RowFactory.create("WARN", "2016-12-31 04:19:32"));
            inMemory.add(RowFactory.create("FATAL", "2016-12-31 03:22:34"));
            inMemory.add(RowFactory.create("WARN", "2016-12-31 03:21:21"));
            inMemory.add(RowFactory.create("INFO", "2015-4-21 04:32:21"));
            inMemory.add(RowFactory.create("FATAL", "2015-4-21 04:23:20"));

            StructField[] fields = {
                    new StructField("level", DataTypes.StringType, false, Metadata.empty()),
                    new StructField("datatime", DataTypes.StringType, false, Metadata.empty()),
            };

            StructType schema = new StructType(fields);
            Dataset<Row> inMemoryDataRows = ss.createDataFrame(inMemory, schema);

            inMemoryDataRows.show();

            long numberOfRows = inMemoryDataRows.count();
            System.out.println("There are " + numberOfRows + " records");

        };
    }

    public static Runnable sparkSqlInMemoryDataGroupingAndAggregation(SparkSession ss) {
        return () -> {

            List<Row> inMemory = new ArrayList<>();

            inMemory.add(RowFactory.create("WARN", "2016-12-31 04:19:32"));
            inMemory.add(RowFactory.create("FATAL", "2016-12-31 03:22:34"));
            inMemory.add(RowFactory.create("WARN", "2016-12-31 03:21:21"));
            inMemory.add(RowFactory.create("INFO", "2015-4-21 04:32:21"));
            inMemory.add(RowFactory.create("FATAL", "2015-4-21 04:23:20"));

            StructField[] fields = {
                    new StructField("level", DataTypes.StringType, false, Metadata.empty()),
                    new StructField("datatime", DataTypes.StringType, false, Metadata.empty()),
            };

            StructType schema = new StructType(fields);
            Dataset<Row> inMemoryDataRows = ss.createDataFrame(inMemory, schema);

            inMemoryDataRows.createOrReplaceTempView("logging_table");

            Dataset<Row> result = ss.sql("select level, collect_list(datatime) from logging_table group by level"); //collect_list est similaire au groupByKey avec les JavaRDD ou JavaPairRDD

            result.show();

            long numberOfRows = result.count();
            System.out.println("There are " + numberOfRows + " records");

        };
    }

    public static Runnable sparkSqlInMemoryDateFormatting(SparkSession ss) {
        return () -> {

            List<Row> inMemory = new ArrayList<>();

            inMemory.add(RowFactory.create("WARN", "2016-12-31 04:19:32"));
            inMemory.add(RowFactory.create("FATAL", "2016-12-31 03:22:34"));
            inMemory.add(RowFactory.create("WARN", "2016-12-31 03:21:21"));
            inMemory.add(RowFactory.create("INFO", "2015-4-21 04:32:21"));
            inMemory.add(RowFactory.create("FATAL", "2015-4-21 04:23:20"));

            StructField[] fields = {
                    new StructField("level", DataTypes.StringType, false, Metadata.empty()),
                    new StructField("datatime", DataTypes.StringType, false, Metadata.empty()),
            };

            StructType schema = new StructType(fields);
            Dataset<Row> inMemoryDataRows = ss.createDataFrame(inMemory, schema);

            inMemoryDataRows.createOrReplaceTempView("logging_table");

            Dataset<Row> result = ss.sql("select level, date_format(datatime, 'MMMM') as month from logging_table");

            result.show();

            long numberOfRows = result.count();
            System.out.println("There are " + numberOfRows + " records");

        };
    }

    public static Runnable sparkSqlInMemoryMultipleGrouping(SparkSession ss) {
        return () -> {

            Dataset<Row> inMemoryDataRows = ss.read()
                    .option("header", true)
                    .csv("src/main/resources/biglog.txt");

            inMemoryDataRows.createOrReplaceTempView("logging_table");

            Dataset<Row> result = ss.sql("select level, date_format(datetime, 'MMMM') as month from logging_table");

            result.createOrReplaceTempView("logging_table");

            Dataset<Row> resultMultipleGrouping = ss.sql("select level, month, count(1) as total from logging_table group by level, month");

            resultMultipleGrouping.show(60);

            long numberOfRows = resultMultipleGrouping.count();
            System.out.println("There are " + numberOfRows + " records");

            resultMultipleGrouping.createOrReplaceTempView("results_table");

            Dataset<Row> totals = ss.sql("select sum(total) from results_table");
            totals.show();


        };
    }

    public static Runnable sparkSqlInMemoryOrdering(SparkSession ss) {
        return () -> {

            Dataset<Row> inMemoryDataRows = ss.read()
                    .option("header", true)
                    .csv("src/main/resources/biglog.txt");

            inMemoryDataRows.createOrReplaceTempView("logging_table");

            Dataset<Row> resultMultipleGrouping = ss.sql("""
                        SELECT level, date_format(datetime, 'MMMM') as month, count(1) as total
                        FROM logging_table
                        GROUP BY level, month
                        ORDER BY CAST(first(date_format(datetime, 'M')) as int), level
                    """);

            resultMultipleGrouping.show(60);

            long numberOfRows = resultMultipleGrouping.count();
            System.out.println("There are " + numberOfRows + " records");
        };
    }

    public static Runnable sparkSqlInMemoryDataFrames(SparkSession ss) {
        return () -> {

            Dataset<Row> dataset = ss.read()
                    .option("header", true)
                    .csv("src/main/resources/biglog.txt");

            dataset = dataset.select(
                    col("level"),
                    date_format(col("datetime"), "MMMM").alias("month"),
                    date_format(col("datetime"), "M").alias("monthnum").cast(DataTypes.IntegerType)
            );

            dataset = dataset.groupBy(col("level"), col("monthnum"), col("month")).count();
            dataset = dataset.orderBy(col("monthnum"), col("level"));
            dataset = dataset.drop(col("monthnum"));
            dataset.show(100);

            long numberOfRows = dataset.count();
            System.out.println("There are " + numberOfRows + " records");
        };
    }

    public static Runnable sparkSqlPivotTable(SparkSession ss) {
        return () -> {

            Dataset<Row> dataset = ss.read()
                    .option("header", true)
                    .csv("src/main/resources/biglog.txt");

            dataset = dataset.select(
                    col("level"),
                    date_format(col("datetime"), "MMMM").alias("month"),
                    date_format(col("datetime"), "M").alias("monthnum").cast(DataTypes.IntegerType)
            );

            Object[] months = new Object[] {"January", "February", "March", "April", "May", "June", "Klex", "July", "August", "September", "October", "November", "December"};
            List<Object> objects = Arrays.asList(months);

            dataset = dataset.groupBy(col("level")).pivot(col("month"), objects).count().na().fill(0);

            dataset.show(100);

            long numberOfRows = dataset.count();
            System.out.println("There are " + numberOfRows + " records");
        };
    }


    public static Runnable sparkSqlAggregation(SparkSession ss) {
        return () -> {

            Dataset<Row> dataset = ss.read()
                    .option("header", true)
                    .csv("src/main/resources/exams/students.csv");

            dataset = dataset.groupBy("subject").agg(
                    max(col("score")).alias("max score"),
                    min(col("score")).alias("min score"),
                    avg(col("score")).alias("avg score")
            );

            dataset.show();

            long numberOfRows = dataset.count();
            System.out.println("There are " + numberOfRows + " records");
        };
    }


    public static Runnable sparkSqlPivotTableWithMultipleAgg(SparkSession ss) {
        return () -> {

            Dataset<Row> dataset = ss.read()
                    .option("header", true)
                    .csv("src/main/resources/exams/students.csv");

            dataset = dataset.groupBy("subject").pivot(col("year")).agg(
                    round(avg(col("score")), 2).alias("average"),
                    round(stddev(col("score")), 2).alias("stddev")
            );

            dataset.show();

            long numberOfRows = dataset.count();
            System.out.println("There are " + numberOfRows + " records");
        };
    }

    public static Runnable sparkSqlLambda(SparkSession ss) {
        return () -> {

            ss.udf().register("hasPass", hasPassUDF(), DataTypes.BooleanType); //UDF with lambda expression

            Dataset<Row> dataset = ss.read()
                    .option("header", true)
                    .csv("src/main/resources/exams/students.csv");

            //dataset = dataset.withColumn("pass", lit(col("grade").equalTo("A+"))); // Without UDF

            dataset = dataset.withColumn("pass", call_udf("hasPass", col("grade"), col("subject"))); //With UDF

            dataset.show();

            long numberOfRows = dataset.count();
            System.out.println("There are " + numberOfRows + " records");
        };
    }

    public static Runnable sparkSqlUnderstandSparkUIGraph(SparkSession ss) {
        return () -> {

            Dataset<Row> dataset = ss.read()
                    .option("header", true)
                    .csv("src/main/resources/biglog.txt");


           /* dataset.createOrReplaceTempView("logging_table");

            Dataset<Row> resultMultipleGrouping = ss.sql("""
                        SELECT level, date_format(datetime, 'MMMM') as month, count(1) as total
                        FROM logging_table
                        GROUP BY level, month
                        ORDER BY CAST(first(date_format(datetime, 'M')) as int), level
                    """);*/

            dataset = dataset.select(
                    col("level"),
                    date_format(col("datetime"), "MMMM").alias("month"),
                    date_format(col("datetime"), "M").alias("monthnum").cast(DataTypes.IntegerType)
            );

            dataset = dataset.groupBy("level", "month", "monthnum").count().alias("total").orderBy("monthnum");
            dataset = dataset.drop("monthnum");

            dataset.show(100);

            long numberOfRows = dataset.count();
            System.out.println("There are " + numberOfRows + " records");

            dataset.explain();
        };
    }

    public static Runnable sparkSqlUdfV2(SparkSession ss) {
        return () -> {

            ss.udf().register("monthStringToNumber", monthStringToNumber(), DataTypes.IntegerType);

            Dataset<Row> dataset = ss.read()
                    .option("header", true)
                    .csv("src/main/resources/biglog.txt");

            dataset.createOrReplaceTempView("logging_table");

            Dataset<Row> results = ss.sql("""
                        SELECT level, date_format(datetime, 'MMMM') as month, count(1) as total
                        FROM logging_table
                        GROUP BY level, month
                        ORDER BY monthStringToNumber(month), level
                    """);


            results.show(100);

            long numberOfRows = results.count();
            System.out.println("There are " + numberOfRows + " records");
        };
    }

    private static UDF1<String, Integer> monthStringToNumber() {

        return month -> {
            /* June Unparsable Exception
            SimpleDateFormat input = new SimpleDateFormat("MMMM");
            SimpleDateFormat output = new SimpleDateFormat("M");Date inputDate = input.parse(month);
            return Integer.parseInt(output.format(inputDate));*/

            switch (month) {
                case "January" -> {
                    return 1;
                }
                case "February" -> {
                    return 2;
                }
                case "March" -> {
                    return 3;
                }
                case "April" -> {
                    return 4;
                }
                case "May" -> {
                    return 5;
                }
                case "June" -> {
                    return 6;
                }
                case "July" -> {
                    return 7;
                }
                case "August" -> {
                    return 8;
                }
                case "September" -> {
                    return 9;
                }
                case "October" -> {
                    return 10;
                }
                case "November" -> {
                    return 11;
                }
                case "December" -> {
                    return 12;
                }
                default -> {
                    return 0;
                }
            }
        };


    }

    @NotNull
    private static UDF2<String, String, Boolean> hasPassUDF() {
        return (grade, subject) -> {
            if(subject.equals("Biology")) {
                return grade.startsWith("A");
            }
            return grade.startsWith("A") || grade.startsWith("B") || grade.startsWith("C");
        };
    }

    private static FilterFunction <Row> modernArtFilter() {
        return row -> row.getAs("subject").equals("Modern Art")
                && Integer.parseInt(row.getAs("year")) >= 2007;
    }
}
